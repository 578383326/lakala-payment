<?php

namespace tonyWa\lklPay\model;

class LabsOrderPreOrderPayReq {
    public $amount;
    public $appId;
    public $exterOrderSource;
    public $mercId;
    public $openId;
    public $orderId;
    public $payMode;
    public $spbillCreateIp;
    public $subject;
    public $termNo;
    public $transType;
}