<?php
namespace tonyWa\lklPay\model;

class BaseRequestVO {
    public $reqData;
    public $termExtInfo;
    public $timestamp;
    public $ver;
}