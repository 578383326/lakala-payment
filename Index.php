<?php
include __DIR__.'/vendor/autoload.php';

use tonyWa\lklPay\model\Lakala;
use GuzzleHttp\Client;

class Index {
    public $apiUrl = "";
    public function run(){
         $params = [
             'appid'                    => '800000010334001',
             'mchSerialNo'              => '48066e7ce3551149cd9ebdaf924582794137feb6',
             'mercId'                   => '822162070120015',
             'termNo'                   => '47827142',
             'merchantPrivateKeyPath'   => './key/pkcs8_private_key.pem',
             'lklCertificatePath'       => './key/lkl-apigw-v2.cer',
         ];
         $lakala = new Lakala($params);
         $res = $lakala->wechat();
         var_dump($res);
    }

    public function createOrder(){
        $this->apiUrl = "https://test.wsmsd.cn/sit/api/v3/labs/trans/preorder";
        $postData = [
            'merchant_no'=>'',
            'term_no'=>'',
            'out_trade_no'=>time(),
            'account_type'=>'WECHAT',
            'trans_type'=>'JSAPI',
            'total_amount'=>0.1,
            'location_info'=>[
                'request_ip'=>"127.0.0.1"
            ],
            'settle_type'=>1,
            'remark'=>'',
            'notify_url'=>'',
            'subject'=>'te'
        ];
    }

}

$index = new Index();
$index->run();








